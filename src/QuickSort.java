import java.util.Random;

class QuickSort <T extends Comparable<T>> {

    private T[] array;

    T[] sort(T[] unsortedArray) {
        if (unsortedArray == null || unsortedArray.length == 0) {
            return unsortedArray;
        }

        this.array = unsortedArray;

        quickSort(0, array.length -1);

        return array;
    }
    

    private void swap(int j, int k) {
        T temp = array[j];
        array[j] = array[k];
        array[k] = temp;
    }

    private int partition(int low, int high) {
        int pivot = selectPivot(low,high);
        swap(pivot,high);
        int pointer = low;
        for(int index = low;index<high;index++){
            if(array[index].compareTo(array[high])< 0){
                pointer++;
                swap(index,pointer-1);
            }
        }
        swap(pointer,high);
        return pointer;

    }


    private int selectPivot(int low, int high) {
        Random random = new Random();
        return random.nextInt((high - low) + 1) + low;
    }

    private void quickSort(int low, int high) {
        if (low < high) {
            int pivot = partition(low,high);
            quickSort(low,pivot-1);
            quickSort(pivot+1,high);
        }
    }

}
