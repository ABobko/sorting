public class Main {
    public static void main(String[] args) {
        Integer[] array = {4, 3, 1, 2, 6, 5, 9, 8, 7, 0};

        StringBuilder before = new StringBuilder();

        for (Integer element : array) {
            before.append(element)
                    .append(" ");
        }

        QuickSort quickSort = new QuickSort();

        try {
            array = (Integer[]) quickSort.sort(array);

            StringBuilder result = new StringBuilder();
            for (Integer element : array) {
                result.append(element)
                        .append(" ");
            }
            System.out.println("before: "
                    + before
                    + "\n"
                    + "result: "
                    + result);
        } catch (ClassCastException e) {
            System.err.println("class cast exeption");
        }
    }
}
